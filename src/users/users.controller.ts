import { Controller, Get, Post, Body, Param, Patch, Delete } from "@nestjs/common";
import { User } from "./user.model";
import { UsersService } from "./users.service";

@Controller('users')
export class UsersController {

  constructor(private readonly usersService: UsersService) {}

  @Post()
  insert(@Body() user: User): number {
    return this.usersService.insert(user);
  }

  @Get()
  findAll(): User[] {
    return this.usersService.findAll();
  }

  @Get(':id')
  findUserById(@Param('id') id: number) {
    return this.usersService.findById(id);
  }

  @Patch(':id')
  update(
    @Param('id') id: number,
    @Body() user: User
  ) {
    this.usersService.update(id, user);
  }

  @Delete(':id')
  delete(@Param('id') id: number) {
    this.usersService.delete(id);
  }

}