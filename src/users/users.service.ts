import { Injectable, NotFoundException, NotAcceptableException } from "@nestjs/common";
import { User } from "./user.model";

@Injectable()
export class UsersService {
  private users: User[] = [];

  insert(user: User): number {
    if (!this.isValidUser(user)) throw new NotAcceptableException('Invalid fields');
    user.id = Math.random();
    this.users.push(user);
    return user.id;
  }

  isValidUser(user: User): boolean {
    return user.name !== undefined &&
    user.email !== undefined &&
    user.password !== undefined &&
    user.name !== '' &&
    user.email !== '' &&
    user.password !== '';
  }

  findAll(): User[] {
    return [...this.users];
  }

  findById(id: number): User {
    return {...this.getUserById(id)};
  }

  update(id: number, user: User) {
    const index = this.findUserIndex(id);
    const foundUser = this.getUserById(id);
    const updateUser = {...foundUser};
    if (user.name) { updateUser.name = user.name; }
    if (user.email) { updateUser.email = user.email; }
    if (user.password) { updateUser.password = user.password; }
    this.users[index] = updateUser;
  }

  delete(id: number) {
    const index = this.findUserIndex(id);
    this.users.splice(index, 1);
  }

  private getUserById(id: number): User {
    const index = this.findUserIndex(id);
    return this.users[index];
  }

  private findUserIndex(id: number): number {
    const index = this.users.findIndex(user => Number(user.id) === Number(id));
    if (index === -1) throw new NotFoundException('User not found');
    return index;
  }

}